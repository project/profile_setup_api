<?php

/**
 * @file
 * Contains Rule module forms for Profile Setup API rules events, conditions & actions
 *
 * A default rule is provided in the profile_setup_api.rules_default.inc file
 */

/**
 * @group - conditions
 */
/**
 * Config form for rules_condition_profile_setup_api_login_count
 */
function rules_condition_profile_setup_api_login_count_form($settings, &$form) {
  $form['settings']['explain'] = array(
    '#type' => 'markup',
    '#value' => t('Each time a user logins the Profile Setup API increases the users login count (how many times the user has logged into the site). Here you can evaluate the login count for the user to decide whether to trigger a route. ie. If login count == 0 (first login), trigger the route.')
  );  
  $form['settings']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Login count is'),
    '#options' => array(
      '==' => t('Equal To (==)'), 
      '!=' => t('Not Equal To (!=)'), 
      '<=' => t('Less Than or Equal To (<=)'),
      '>=' => t('Greater Than or Equal To (>=)'),
      '<' => t('Less Than (<)'),
      '>' => t('Greater than (>)')
    ),
    '#default_value' => isset($settings['operation']) ? $settings['operation'] : '==',
    '#prefix' => '<div class="container-inline">',
  );
  $form['settings']['login_count'] = array(
    '#type' => 'textfield',
    '#title' => '',
    '#size' => 5,
    '#default_value' => isset($settings['login_count']) ? $settings['login_count'] : '0',
    '#suffix' => '</div>'
  );
}

/**
 * @group - actions
 */
function rules_action_profile_setup_api_trigger_route_form($settings, &$form) {
  $all_routes = profile_setup_api_load_routes();
  if (empty($all_routes)) {
    //TODO - add link to add new route
    $form['route'] = array(
      '#type' => 'markup',
      '#value' => t('You must create some routes before you can use this action.')
    );
    return $form;
  }
  $routes[0] = '';
  foreach ($all_routes as $rid => $route) {
    $routes[$rid] = $route->name .' ('. $route->machine_name .')';
  }
  //print_r($settings);
  $form['settings']['route'] = array(
    '#type' => 'select',
    '#title' => t('Select route to trigger'),
    '#options' => $routes,
    '#default_value' => isset($settings['route']) ? $settings['route'] : array(),
    '#required' => TRUE,
  );  
  return $form;
}

/**
 * Evaluate whether select route contains any steps
 */
function rules_action_profile_setup_api_trigger_route_validate($form, $form_state) {
  $rid = $form_state['values']['settings']['route'];
  $route = profile_setup_api_load_route($rid, TRUE);
  if (empty($route->steps)) {
    form_set_error('route', t('The selected route does not contain any steps, please <a href="@add_steps">add some steps</a> to the route first.', array('@add_steps' => url('admin/user/profile_setup_api/route-steps/'. $rid))));
  }
}
