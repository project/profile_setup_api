<?php

/**
 * @file
 * Contains Rule module interaction for Profile Setup API
 * Provides:
 * 3 Events - when user naivgates through route form steps
 * 1 Condition - Profile Setup API Login Count
 * 1 Action - Trigger Profile Setup API Route
 *
 * A default rule is provided in the profile_setup_api.rules_default.inc file
 */

/**
 * @group - events
 */
/**
 * Implementation of hook_rules_event_info().
 */
function profile_setup_api_rules_event_info() {
  return array(
    'rules_event_profile_setup_api_route_finished' => array(
      'label' => t('Profile Setup API route finished'),
      'module' => 'Profile Setup API',
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('User who finished the route.')),
      ),
    ),
  );
}

/**
 * @group - conditions
 */
/**
 * Implementation of hook_rules_condition_info().
 */
function profile_setup_api_rules_condition_info() {
  return array(
    'rules_condition_profile_setup_api_login_count' => array(
      'label' => t('Profile Setup API login count'),
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('User whos login count should be evaluated.')),
      ),
      'help' => t('Evaluates to TRUE, if user loin count matches conditions set.'),
      'module' => 'Profile Setup API',
    ),
  );
}

/**
 * Condition login count: condition to check users login count
 */
function rules_condition_profile_setup_api_login_count($user, $settings) {
  $operation = $settings['operation'];
  $count = $settings['login_count'];
  $user_count = $user->profile_setup_api_user_login_count;

	//if == to zero use empty()
	if ($count == 0 && $operation == '==') {
		return empty($user->profile_setup_api_user_login_count);
	}
	
  //use eval as it's the only ? 
  return eval("if (" . $user_count . $operation . $count . ") { return true; } else { return false; }");
}

/**
 * @group - actions
 */
/**
 * Implementation of hook_rules_action_info().
 * TODO - check some routes exist
 */
function profile_setup_api_rules_action_info() {
  return array(
    'rules_action_profile_setup_api_trigger_route' => array(
      'label' => t('Trigger a route'),
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('User who route should be trigger for')),
      ),
      'module' => 'Profile Setup API',
    ),
    'rules_action_profile_setup_api_increment_logins' => array(
      'label' => t('Increment user login count'),
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('User who should have their login count incremented.')),
      ),
      'module' => 'Profile Setup API',
    ),		
  );
}

/**
 * Action route trigger: condition met so trigger a given route
 */
function rules_action_profile_setup_api_trigger_route($user, $settings) {
  $rid = $settings['route'];
  $route = profile_setup_api_load_route($rid, TRUE);
  //no steps do nothing - validation should catch this unless user deletes steps after
  if (empty($route->steps)) {
    return;
  }
  // This lets _rules_action_drupal_goto_handler() invoke drupal_goto before the page is output.
  $path = $route->route_url;
	$_REQUEST['destination'] = urlencode($path);
  $GLOBALS['_rules_action_drupal_goto_do'] = TRUE;
}

/**
 * Action route trigger: condition met so trigger a given route
 */
function rules_action_profile_setup_api_increment_logins($account, $settings) {
	return profile_setup_api_update_user_login_count($account);
}