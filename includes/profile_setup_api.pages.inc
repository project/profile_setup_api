<?php

/**
 * @file
 * Holds all pages related functions for Profile Setup API
 */

/**
 * Catch all for all route pages
 */
function profile_setup_api_route_page($route) {
  //no steps redirect to finish page
  if (empty($route->steps)) {
    drupal_goto(($route->route_bailout_destination ? $route->route_bailout_destination : 'user'));
  }  
  
  global $user;
  $account = user_load($user->uid); //load full user object
  
  // required includes for wizard
  ctools_include('wizard');
  ctools_include('object-cache');  
  
  //use step data to construction $form_info
  $count = 1;
  $form_arguments = array();
  foreach ($route->steps as $step_key => $step) {
    if ($step['step']['url'] && $step['step']['name'] && $step['step']['form']['form_id']) {
      if ($count == 1) {
        $first_step = $step['step']['url'];
      }
      $order[$step['step']['url']] = t('!count: !name', array('!count' => $count, '!name' => check_plain($step['step']['name'])));
      $forms[$step['step']['url']] = array('form id' => $step['step']['form']['form_id']);
      
      //allow modules to add some optional extras
      if ($step['step']['include']) {
        $forms[$step['step']['url']]['include'] = $step['step']['include'];
      }
      if ($step['step']['form']['form_arguments']) {
        $form_arguments += $step['step']['form']['form_arguments'];
      }
      ++$count;
    }
  }

  //work page current set based on route_url
  $args = explode('/', $route->route_url);
  $args_count = count($args) - 1;
  $step_path = arg($args_count+1);
  $current_step = empty($step_path) ? current(array_keys($order)) : $step_path;
  
  $form_info = array(
    'id'              => 'profile_setup_api_route_'. $route->rid,
    'path'            => $route->route_url ."/%step",
    'return path'     => $route->route_destination,
    'cancel path'     => $route->route_bailout_destination,
    'show trail'      => $route->route_disable_trail ? FALSE : TRUE,
    'show back'       => $route->route_disable_back ? FALSE : TRUE,
    'show cancel'     => $route->route_disable_cancel ? FALSE : TRUE,
    'show return'     => FALSE, 
    'next text'       => t('Next step'),
    'next callback'   => 'profile_setup_api_route_page_form_next',
    'finish callback' => 'profile_setup_api_route_page_form_finish',
    'cancel callback' => 'profile_setup_api_route_page_form_cancel',
    'order'           => $order,
    'forms'           => $forms,
  );
  //print_rr($form_info);
  //invoke drupal_alter (hook_profile_setup_api_route_form_info_alter) to allow modules to alter $form_info
  drupal_alter('profile_setup_api_route_form_info', $form_info, $route, $account);

  $form_state = array(
    'cache name' => NULL,
  );
  // no matter the step, you will load your values from the callback page
  $cache = profile_setup_api_route_get_page_cache(NULL, $route);
  if (!$cache) {
    $cache = new stdClass();
    // ** set the storage object so its ready for whatever comes next
    ctools_object_cache_set('profile_setup_api_route_'. $route->rid, $form_state['cache name'], $cache);
  }
    
  $form_state['cache_obj'] = $cache;
  $form_state['account'] = $account;
  $form_state['route'] = $route;
  $form_state['form_arguments'] = $form_arguments;
	
	//TODO - move to theme function?
  $output = '<div class="psapi-route psapi-route-step-name-'. $current_step .'" id="psapi-route-'. $route->rid .'">';
  //intro if one has been set
  if ($route->route_intro && $_GET['q'] == $route->route_url) {
    $output .= '<div class="psapi-route-intro">'. check_markup($route->route_intro, $route->route_intro_format) .'</div>';
  }  
  
  $output .= ctools_wizard_multistep_form($form_info, $current_step, $form_state);
	$output .= '</div>';
  return $output;
}

/**
 * Get the cached changes to a given task handler.
 */
function profile_setup_api_route_get_page_cache($name, $route) {
  $cache = ctools_object_cache_get('profile_setup_api_route_'. $route->rid, $name);
  return $cache;
}

/**
 * Callback generated when next button is clicked
 * Steps should provide their own _validate & _submit hooks
 * This is a catch all to invoke the Profile Setup API
 *
 * TODO: 
 * 1) Invoke API so other modules can save the data from this step
 * 2) Invoke rules API 
 */
function profile_setup_api_route_page_form_next(&$form_state) {
  
}

/**
 * Callback generated when the add page process is finished.
 * this is where you'd normally save.
 * TODO: 
 * 1) Invoke API so other modules can save the full data collected
 * 2) Invoke rules API 
 */
function profile_setup_api_route_page_form_finish(&$form_state) {
  $route = &$form_state['route'];
  
  //outro if one has been set
  if ($route->route_outro) {
    drupal_set_message(check_markup($route->route_outro, $route->route_outro_format));
  }
  
  // Clear the cache
  ctools_object_cache_clear('profile_setup_api_route_'. $route->rid, $form_state['cache name']);
  if (empty($form_state['redirect'])) {
    $form_state['redirect'] = $route->route_destination ? $route->route_destination : 'user';
  }
}

/**
 * Callback generated when the 'cancel' button is clicked.
 *
 * All we do here is clear the cache.
 * TODO: 
 * 1) Invoke API so other modules can react
 * 2) Invoke rules API
 */
function profile_setup_api_route_page_form_cancel(&$form_state) {
  $route = &$form_state['route'];
 
  //outro if one has been set
  if ($route->route_bailout_outro) {
    drupal_set_message(check_markup($route->route_bailout_outro, $route->route_bailout_outro_format));
  }
  
  // Clear the cache
  ctools_object_cache_clear('profile_setup_api_route_'. $route->rid, $form_state['cache name']);
  if (empty($form_state['redirect'])) {
    $form_state['redirect'] = $route->route_bailout_destination ? $route->route_bailout_destination : 'user';
  }
}