<?php

/**
 * @file
 * Holds all admin related functions for Profile Setup API
 */

/**
 * Admin overview page
 * TODO: Implement help module for help
 */
function profile_setup_api_admin_overview() {
  //routes
  $output = '<h3>'. t('Profile Setup Routes') .'</h3>';
  $tags = profile_setup_api_load_routes(TRUE, 'tag', TRUE);
  //print_r($routes);
  if (empty($tags)) {
    $output .= t('No Profile Setup Routes have been found. <a href="@add_link_url">Add a new route</a>.', array('@add_link_url' => url('admin/user/profile_setup_api/route-add')));
  }
  else {
    $header = array(t('Route'), t('Route URL'), t('Operations')); 
    foreach ($tags as $tag => $routes) {
      $rows[] = array(array('data' => $tag, 'colspan' => count($header) + 1));
      foreach ($routes as $route) {
        $operations = array();
        $operation = array();
        $operation[] = l(t('Edit'), 'admin/user/profile_setup_api/route-edit/'. $route->rid);
				$operation[] = l(t('Delete'), 'admin/user/profile_setup_api/route-delete/'. $route->rid);
        $operation[] = l(t('Add/Edit Route Steps'), 'admin/user/profile_setup_api/route-steps/'. $route->rid) .' ('. format_plural(count($route->steps), '1 step', '@count steps') .')';
        $operations = implode(' | ', $operation);
        $route_summary = '<div class="route-name">'. $route->name .' ('. $route->machine_name .')</div>';
        $route_summary .= '<div class="route-description">'. $route->description .'</div>';
        $rows[] = array($route_summary, l($route->route_url, $route->route_url), $operations);
      }
    }
    $output .= theme('table', $header, $rows, $attributes = array());    
  }
  
  //available steps
  $output .= '<h3>'. t('Available Profile Setup Steps') .'</h3>';
  $output .= t('Profile Setup Steps are created in code & provided by other modules, below are steps currently available for you to add to your routes');
  $steps = profile_setup_api_load_steps();
  if (empty($steps)) {
    $output .= t('No Profile Setup Steps defined, you need to enable at least the Profile Setup API Defaults module.');
  }
  else {
    $rows = array();
    $header = array(t('Name'), t('Description'), t('Module')); //TODO: List routes step is used in
    foreach ($steps as $step_key => $step) {
      $rows[] = array(($step['admin_name'] ? $step['admin_name'] : $step['name']), $step['description'], $step['module']);
    }
    $output .= theme('table', $header, $rows, $attributes = array());
  }
  
  return $output;
}

/**
 * provides edit & add route form
 * TODO: allow for dynamically tokens in destination & bailout urls
 */
function profile_setup_api_route_form($form_state, $route = '') {
  $form_state['route'] = $route;
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $route->rid,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#required' => 1,
    '#title' => 'Route title/name',
    '#default_value' => $route->name,
		'#description' => t('Displayed as the page title throughout the set up route.')
  );
  $form['machine_name'] = array(
    '#type' => 'textfield',
    '#required' => 1,
    '#title' => 'Machine name',
    '#default_value' => $route->machine_name,
		'#description' => t('Specify a unique name containing only alphanumeric characters, and underscores.'),
  );		
  $form['description'] = array(
    '#title' => 'Description',
    '#type' => 'textarea',
    '#max_length' => 255,
    '#default_value' => $route->description,
  );
  $form['tag'] = array(
    '#type' => 'textfield',
    '#title' => 'Route tag',
    '#default_value' => $route->tag,
  );
  $form['route_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Route URL'),
    '#required' => 1,
    '#size' => 40,
    '#description' => t('The URL of the route ie. profile-setup, steps will be added to this like profile-setup/step1'),
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#default_value' => $route->route_url,
  );
  
  //route form options - saved in data field, render in $route->data
  $form['data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Route Form Options'),
    '#description' => t('Here you can customize how the forms with this route function.'),
    '#tree' => TRUE,
  );
  $form['data']['route_destination'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#title' => t('Route Destination'),
    '#description' => t('Where should users be redirected once they complete this setup route? Default will redirect to a users profile page.'),
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#default_value' => $route->route_destination,
  );
  $form['data']['route_bailout_destination'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#title' => t('Bail Out Destination'),
    '#description' => t('Where should users be redirected if they click the cancel button during the setup route? Default will redirect to a users profile page.'),
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#default_value' => $route->route_bailout_destination,
  );
  $form['data']['route_intro'] = array(
    '#type' => 'textarea',
    '#title' => t('Route Introduction'),
    '#description' => t('Optional text which you can display to users when they enter the first page of the setup route.'),
    '#default_value' => $route->route_intro,
  );
  $form['data']['route_intro_format'] = filter_form($node->route_intro_format, NULL, array('route_intro'));
  $form['data']['route_outro'] = array(
    '#type' => 'textarea',
    '#title' => t('Route Outro'),
    '#description' => t('Optional text which you can display to users when they finish the setup route. Shown as a message on the preceeding destination page.'),
    '#default_value' => $route->route_outro
  );
  $form['data']['route_outro_format'] = filter_form($node->route_intro_format, NULL, array('route_outro'));
  $form['data']['route_bailout_outro'] = array(
    '#type' => 'textarea',
    '#title' => t('Route Bail Out Message'),
    '#description' => t('Optional text which you can display to users if they click the cancel button during the setup route. Shown as a message on the preceeding page.'),
    '#default_value' => $route->route_bailout_outro,
  );
  $form['data']['route_bailout_outro_format'] = filter_form($node->route_intro_format, NULL, array('route_bailout_outro'));

  //form buttons
  $form['data']['route_disable_cancel'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable the "Cancel" button on route form?'),
    '#default_value' => $route->route_disable_cancel,
    '#description' => t('You can choose to disable the display of a "Cancel" button on the route form steps.')
  );
  $form['data']['route_disable_back'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable the "Back" button on route form?'),
    '#default_value' => $route->route_disable_back,
    '#description' => t('You can choose to disable the display of a "Back" button on the route form steps.')
  );  
  $form['data']['route_disable_trail'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable the route step trail from display'),
    '#default_value' => $route->route_disable_trail,
    '#description' => t('You can choose to disable the display of form step trail on the route form steps.')
  );     
    
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save route'),
  );  
  return $form; 
}

/**
 * Validate route submission
 * 1. Check for name clashes
 * 2. Check for URL clashes
 */
function profile_setup_api_route_form_validate($form, &$form_state) {
  $route = $form_state['route'];
  if (db_result(db_query("SELECT COUNT(rid) FROM {profile_setup_api_routes} WHERE machine_name = '%s' AND rid <> %d", $form_state['values']['machine_name'], $form_state['values']['rid'])) > 0) {
    form_set_error('name', t('There is already a route with that machine name, please input a different machine name.'));
  }

  // Validate route_url, destination & bailout_destination
  $paths['route_url'] = array('link_path' => $form_state['values']['route_url']);
  $paths['data][route_destination'] = array('link_path' => $form_state['values']['data']['route_destination']);
  $paths['data][route_bailout_destination'] = array('link_path' => $form_state['values']['data']['route_bailout_destination']);
  foreach ($paths as $form_key => $path) {
    if (empty($path['link_path'])) {
      continue;
    }
    $normal_path = drupal_get_normal_path($path['link_path']);
    if ($path['link_path'] != $normal_path) {
      drupal_set_message(t('The menu system stores system paths only, but will use the URL alias for display. %link_path has been stored as %normal_path', array('%link_path' => $path['link_path'], '%normal_path' => $normal_path)));
      $path['link_path'] = $normal_path;
    }
  
    //check route_url aren't already in use
    if ($form_key == 'route_url') {
      if ($route->{$form_key} != $path['link_path']) {
        if ($router_item = menu_get_item($path['link_path'])) {
          form_set_error($form_key, t("The path '@path' is already in use, please select a different path.", array('@path' => $path['link_path'])));
        }
      }
    }
    else {
      if (!empty($path) && !menu_valid_path($path)) {
        form_set_error($form_key, t("The path '@path' is either invalid or you do not have access to it.", array('@path' => $path['link_path'])));
      }        
    }
  }
  cache_clear_all();
  module_invoke('menu', 'rebuild');
}

/**
 * submit route form values for saving to DB
 */
function profile_setup_api_route_form_submit($form, &$form_state) {
  if (!form_get_errors()) {
    $route = (object) $form_state['values'];
    if (profile_setup_api_save_route($route)) {
      drupal_set_message(t('The route has been saved.'));
    }
    $form_state['redirect'] = 'admin/user/profile_setup_api';
    return;
  }
}

/**
 * delete a route confirmation form
 */
function profile_setup_api_route_confirm_delete_form(&$form_state, $route) {
	$form['route'] = array(
    '#type' => 'hidden',
    '#value' => $route,
  );
	return confirm_form($form, t('Are you sure you want to delete the route %name?', array('%name' => $route->name)), 'admin/user/profile_setup_api', '', t('Delete'), t('Cancel'));
}

/**
 * delete a route confirmation form submit
 */
function profile_setup_api_route_confirm_delete_form_submit($form, &$form_state) {
	profile_setup_api_delete_route($form_state['values']['route']);
	$form_state['redirect'] = 'admin/user/profile_setup_api';
}

/**
 * Steps admin page where steps can be assigned to a route
 */
function profile_setup_api_route_steps_form($form_state, $route = '') {
  
  $form = array('#tree' => TRUE);
  $form['#route'] = $route;
  if (!empty($route->steps)) {
    foreach ($route->steps as $step_key => $data) {
      $form['steps'][$step_key]['#step'] = $data['step'];
      $form['steps'][$step_key]['step_key'] = array('#value' => $step_key);
      $form['steps'][$step_key]['name'] = array('#value' => ($data['step']['admin_name'] ? $data['step']['admin_name'] : $data['step']['name']));
      $form['steps'][$step_key]['description'] = array('#value' => $data['step']['description']);
      $form['steps'][$step_key]['remove'] = array('#value' => l(t('Remove step'), 'admin/user/profile_setup_api/route-steps/'. $route->rid .'/delete/'. $step_key, array('attributes' => array('class' => 'step-remove', 'title' => t('Remove step from this route')))));
      if (profile_setup_api_route_step_has_settings($data['step'])) {
        $form['steps'][$step_key]['settings'] = array('#value' => l(t('Settings'), 'admin/user/profile_setup_api/route-steps/'. $route->rid .'/settings/'. $step_key, array('attributes' => array('class' => 'step-settings', 'title' => t('Remove step from this route')))));
      }
      $form['steps'][$step_key]['position'] = array(
        '#type' => 'textfield',
        '#size' => 5,
        '#default_value' => $data['position'],
        '#attributes' => array(
          'class' => 'step-position',
        ),
      );
    }
    $form['route_steps_order_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Step Order'),
      '#submit' => array('profile_setup_api_route_steps_form_order_submit'),
    );
  }
  
  // add a dropdown for adding steps to route
  $all_steps = profile_setup_api_load_steps();
  $form['#all_steps'] = $all_steps;
  $step_options[] = '- '. t('None') .' -';
  foreach ($all_steps as $step_key => $step) {
    $step_options[$step_key] = ($step['admin_name'] ? $step['admin_name'] : $step['name']);
    if ($route->steps[$step_key]) {//remove alrady added steps
      unset($step_options[$step_key]);
    }
  }
  
  $form['add'] = array(
    '#type'   => 'fieldset',
    '#collaspible' => FALSE,
    '#title' => t('Add a new step to this route'),
  );
  if (count($step_options) > 1) {
    $form['add']['step_key'] = array(
      '#type' => 'select',
      '#title' => t('Select a step to add to this route'),
      '#options' => $step_options,
      '#default_value' => '',
    );
    $form['add']['step_add'] = array(
      '#type' => 'submit',
      '#value' => t('Add Step'),
      '#submit' => array('profile_setup_api_route_steps_form_add_submit'),
    );
  }
  else {
    $form['add']['#description'] = t('This route already contains all the available steps you can add.');
  }
  
  return $form;
}

/**
 * form submit to add step to route
 */
function profile_setup_api_route_steps_form_add_submit($form, &$form_state) {
  $route = $form['#route'];
  $all_steps = $form['#all_steps'];
  if (!empty($form_state['values']['add']['step_key'])) {
    if (profile_setup_api_save_route_steps('save_single', $route, $form_state['values']['add']['step_key'])) {
      drupal_set_message(t('Step was successfully added to this route.'));
      return TRUE;
    }
  }
  drupal_set_message(t('No step was selected to add to this route.'), 'error');
}

/**
 * form submit to order steps in a route
 */
function profile_setup_api_route_steps_form_order_submit($form, &$form_state) {
  $route = $form['#route'];
  $all_steps = $form['#all_steps'];
  if ($form_state['values']['steps']) {  
    if (profile_setup_api_save_route_steps('order', $route, $form_state['values']['steps'])) {
      drupal_set_message(t('Steps successfully reordered.'));
      return TRUE;
    }
  }
  drupal_set_message(t('The steps could not be reordered.'), 'error');
}

/**
 * Steps admin page where steps can be assigned to a route
 */
function profile_setup_api_route_steps_delete($route = '') {
  $type = 'status';
  $step_key = arg(6);
  if (empty($route) || empty($step_key)) {
    $type = 'error';
    $message = t('Step could not be removed from route.');
  }
  if (profile_setup_api_save_route_steps('delete', $route, $step_key)) {
    $message = t('Step successfully removed from route.');
  }
  drupal_set_message($message, $type);
  drupal_goto('admin/user/profile_setup_api/route-steps/'. $route->rid);
}

/**
 * Steps admin settings page
 */
function profile_setup_api_route_steps_settings($route = '', $step_key = '') {
  print_r($route);
	return t('TODO - Step settings forms.');
}

/**
 * Theme the route steps admin list as a sortable list.
 *
 * @ingroup themeable
 */
function theme_profile_setup_api_route_steps_form($form) {
  $output = '';

  drupal_add_tabledrag('profile-setup-api-step-dragdrop', 'order', 'sibling', 'step-position');
  $js = "
    $('a.step-remove').click(function() {
      if (confirm('". t('Are you sure you want to remove this step from the route?') ."')) {
        return true;
      }
      else {
        return false;
      };
    });
  ";
  drupal_add_js($js, 'inline', 'footer');

  // render form as table rows
  $rows = array();
  $counter = 1;
  foreach (element_children($form['steps']) as $key) {
    if (isset($form['steps'][$key]['name'])) {
      $step_key = $form['steps'][$key]['step_key']['#value'];
      $row = array();
      $row[] = drupal_render($form['steps'][$key]['name']) .' ['. drupal_render($form['steps'][$key]['step_key']) .']';
      $row[] = drupal_render($form['steps'][$key]['description']);
      $row[] = drupal_render($form['steps'][$key]['position']);
      $row[] = drupal_render($form['steps'][$key]['remove']) . ($form['steps'][$key]['settings'] ? ' | '. drupal_render($form['steps'][$key]['settings']) : '');
      $rows[] = array(
        'data'  => $row,
        'class' => 'draggable',
      );
    }

    $counter++;
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No steps define for this route'), 'colspan' => 5));
  }

  // render the main nodequeue table
  $header = array(t('Step Name'), t('Description'), t('Position'), t('Operations'));
  $output .= theme('table', $header, $rows, array('id' => 'profile-setup-api-step-dragdrop', 'class' => 'profile-setup-api-step-dragdrop'));

  // render the remaining form elements
  $output .= drupal_render($form);

  return $output;
}
