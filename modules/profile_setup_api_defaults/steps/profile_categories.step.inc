<?php

/**
 * @file
 * Holds Profile category form steps for Profile Setup API defaults module
 */

/**
 * catch all for profile category form
 */
function psapi_defaults_profile_category_form(&$form, &$form_state) {
	$account = $form_state['account'];
	$category = $form_state['step']; 
	$edit = (array) $account;
	
	$form += _user_forms($edit, $account, $category);
  $form['_category'] = array(
    '#type' => 'value',
    '#value' => $category,
  );
  $form['_account'] = array(
    '#type' => 'value',
    '#value' => $account,
  );

	
	return $form;
}

/**
 * catch all for profile category form validation
 */
function psapi_defaults_profile_category_form_validate(&$form, &$form_state) {
	$account = $form_state['values']['_account'];
	$edit = empty($form_state['values']) ? (array) $account : $form_state['values'];
	profile_validate_profile($edit, $form_state['values']['_category']);
}

/**
 * catch all for profile category form submits
 */
function psapi_defaults_profile_category_form_submit(&$form, &$form_state) {
  $account = $form_state['values']['_account'];
	$edit = empty($form_state['values']) ? (array) $account : $form_state['values'];	
	profile_save_profile($edit, $account, $form_state['values']['_category']);
}