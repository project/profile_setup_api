<?php

/**
 * @file
 * Holds Profile Setup API form step for profile picture upload
 */

/**
 * profile picture upload form
 */
function psapi_defaults_profile_picture_form(&$form, &$form_state) {
  $account = $form_state['account'];
  $user_form = user_edit_form($form_state, $account->uid, $edit); //use default   
	$form['picture'] = $user_form['picture'];
  
	//just incase of a custom reg custom allowing upload
	if ($account->uid && $picture = theme('user_picture', (object) $account)) {
	  $form['picture']['current_picture'] = array('#value' => $picture, '#weight' => -10000);	
	}
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $form['#account'] = array(//do i need this?
    '#type' => 'value',
    '#value' => $account,
  );
  return $form;
}

/**
 * profile picture upload form submit
 */
function psapi_defaults_profile_picture_form_submit(&$form, &$form_state) {
  // If required, validate the uploaded picture.
  $validators = array(
    'file_validate_is_image' => array(),
    'file_validate_image_resolution' => array(variable_get('user_picture_dimensions', '85x85')),
    'file_validate_size' => array(variable_get('user_picture_file_size', '30') * 1024),
  );
  
  $account = $form_state['account'];
  if ($file = file_save_upload('picture_upload', $validators)) {
    if (!_profile_setup_profile_picture_validate($file, $account)) {
      return FALSE;
    }
    else {
      drupal_set_message(t('Your profile picture was successfully uploaded'));
      return TRUE;
    }
  }
  else {
    return TRUE;
  }    
}

/**
 * custom profile picture upload validate
 * Bulk taken from user.module
 */
function _profile_setup_profile_picture_validate($file, $account) {
  global $user;
  // Check that uploaded file is an image, with a maximum file size
  // and maximum height/width.
  $info = image_get_info($file->filepath);
  list($maxwidth, $maxheight) = explode('x', variable_get('user_picture_dimensions', '85x85'));

  if (!$info || !$info['extension']) {
    form_set_error('picture_upload', t('The uploaded file was not an image.'));
    return FALSE;
  }
  elseif (image_get_toolkit()) {
    image_scale($file->filepath, $file->filepath, $maxwidth, $maxheight);
  }
  elseif (filesize($file->filepath) > (variable_get('user_picture_file_size', '30') * 1000)) {
    form_set_error('picture_upload', t('The uploaded image is too large; the maximum file size is %size kB.', array('%size' => variable_get('user_picture_file_size', '30'))));
    return FALSE;
  }
  elseif ($info['width'] > $maxwidth || $info['height'] > $maxheight) {
    form_set_error('picture_upload', t('The uploaded image is too large; the maximum dimensions are %dimensions pixels.', array('%dimensions' => variable_get('user_picture_dimensions', '85x85'))));
    return FALSE;
  }

  //Rename picture with unique id - taken from unique_avatar.module
  $dest = variable_get('user_picture_path', 'pictures') .'/picture-'. $user->uid .'-'. md5(time()) .'.'. $info['extension'];
  if (file_move($file->filepath, $dest)) {
    //Clean up existing picture
    if (module_exists('imagecache')) {
      imagecache_image_flush($user->picture);
    }
    file_delete($user->picture);

    //Assign new picture
    $edit['picture'] = file_directory_path() .'/'. $dest;
    $user->picture = file_directory_path() .'/'. $dest;
    db_query("UPDATE {users} SET picture = '%s' WHERE uid = %d", $user->picture, $user->uid);
    return TRUE;
  }
  return FALSE;
}