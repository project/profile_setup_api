<?php

/**
 * @file
 * Holds Profile category form steps for Profile Setup API defaults module
 * Inspired by http://shvetsgroup.com/blog/multistep-registration-form-drupal%C2%A06
 */

/**
 * catch all for profile category form
 */
function psapi_defaults_content_profile_form(&$form, &$form_state) {
  $account = $form_state['account'];
  $route = $form_state['route'];
  $type = str_replace('-', '_', strtolower($form_state['step']));
  
  //load in existing content profile if exists (unlikely)
  if ($profile = content_profile_load($type, $account->uid)) {
    $node = (array)$profile;
  }
  else {
    $node = array('uid' => $account->uid, 'name' => $account->name, 'type' => $type);
  }
  
  $form['#node'] = $node;
  
  // Create an additional node form. 
  $node_form = drupal_retrieve_form($type .'_node_form', $form_state, $node);
  drupal_prepare_form($type .'_node_form', $node_form, $form_state);
 
  $node_form += array('#field_info' => array());
  $form_add = array();
  $fields = array();
  $hidden_fields = profile_setup_api_defaults_content_profile_get_hidden_fields($route);
  
  // If form elements not associated with CCK are not added, copy 
  // only CCK fields. 
  if (in_array('other', $hidden_fields)) {
    foreach ($node_form['#field_info'] as $field_name => $info) {
      if (isset($node_form[$field_name])) {
        $form_add[$field_name] = $node_form[$field_name];
      }
    }
    // Copy the field groups. 
    $keys = array_keys($node_form);
    foreach ($keys as $key) {
      if (stristr($key, 'group_')) {
        $form_add[$key] = $node_form[$key];
      }
    }
    // Add title 
    $form_add['title'] = $node_form['title'];
 
    // Set these values equal to the value of the node (from 
    // the auxiliary form) as it can be necessary for 
    // #ahah callbacks.
    $form_add['#node'] = $node_form['#node'];
    $form_add['#parameters'] = $node_form['#parameters'];
    $form_add['type'] = $node_form['type'];
  }
  else {
    foreach (array('buttons', 'language', '#theme', 'options') as $key) {
      unset($node_form[$key]);
    }
    $form_add = $node_form;
  }

  // Hide fields as configured
  foreach ($hidden_fields as $field_name) {
    if (module_exists('fieldgroup') && ($group_name = _fieldgroup_field_get_group($type, $field_name))) {
      unset($form_add[$group_name][$field_name]);
      if (count(element_children($form_add[$group_name])) == 0) {
        unset($form_add[$group_name]);
      }
    }
    else {
      unset($form_add[$field_name]);
    }
  }
 
  // Add new elements to the form $form. 
  $form += array('#field_info' => array());
  $form['#field_info'] += $node_form['#field_info'];
  $form += $form_add;

  if (isset($node_form['#attributes']['enctype'])) {
    $form['#attributes']['enctype'] = $node_form['#attributes']['enctype'];
  }
	
  return $form;
}

/**
 * catch all for profile category form validation
 * borrows a lot from content_profile_registration_user_register_validate in content_profile_registration.module
 */
function psapi_defaults_content_profile_form_validate(&$form, &$form_state) {
  node_validate($form_state['values'], $form);
}

/**
 * catch all for profile category form submits
 */
function psapi_defaults_content_profile_form_submit(&$form, &$form_state) {
	//print_rr($form_state['values']);
	$node = node_submit($form_state['values']);
  $form_state['node'] = (array) $node;
  //print_rr($node);
	node_save($node);
	 
  // Give us a nice log message.
  if ($node->nid) {
    watchdog('Profile Setup', 'Content Profile: added %user %type upon profile setup API route.', array('%user' => $node->name, '%type' => $type), WATCHDOG_NOTICE, l(t('view'), "node/$node->nid"));
  }
}