<?php

/**
 * @file
 */

/**
 * Implementation of hook_schema().
 */
function profile_setup_api_schema() {
  $schema['profile_setup_api_routes'] = array(
    'description' => t('Base table for all profile setup API routes.'),
    'fields' => array(
      'rid' => array(
        'description' => t('The primary identifier for a route.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => t('A name for the route.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'machine_name' => array(
       'description' => t('A unique machine name for the route'),
       'type' => 'varchar',
       'length' => '255',
       'not null' => TRUE,
      ),      
      'description' => array(
        'description' => t('Optional description of the route.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
      'tag' => array(
        'description' => t('Optional tag to help organize routes.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
      'route_url' => array(
        'description' => t('Menu URL this is accessible at.'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'data' => array(
        'description' => t('Hold any additional data about the step.'),
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => t('On/off switch for route.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
    ),
    'primary key' => array('rid'),
    'unique keys' => array(
      'machine_name' => array('machine_name')
    ),
  );
  
  //holds route to step relation
  $schema['profile_setup_api_routes_steps'] = array(
    'description' => t('Table holds route to step relation'),
    'fields' => array(
      'rid' => array(
        'description' => t('Route Id the step belongs to'),
        'type' => 'int',
        'not null' => TRUE,
      ),
      'step_key' => array(
        'description' => t('Step Key of the related step'),
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'position' => array(
        'description' => t('Position or weight of the step in the route '),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'indexes' => array(
      'rid' => array('rid', 'step_key'),
    ),
  );


  return $schema;
}

/**
 * Implementation of hook_install().
 */
function profile_setup_api_install() {
  drupal_install_schema('profile_setup_api');
  //make module be called last so the registration redirection doesn't mess with other modules
  db_query("UPDATE {system} SET weight = 100 WHERE name = 'profile_setup_api'");
}

/**
 * Implementation of hook_uninstall().
 */
function profile_setup_api_uninstall() {
  // Remove tables.
  drupal_uninstall_schema('profile_setup_api');
  /*variable_del('profile_setup_api_oncomplete_redirect');
  variable_del('profile_setup_api_category_exclude');
  variable_del('profile_setup_api_change_username');
  variable_del('profile_setup_api_step_order');
  variable_del('profile_setup_api_use_js_loaders');
  variable_del('profile_setup_api_user_pictures');
  //$result = db_query("SELECT name FROM {variable} WHERE name LIKE 'profile_setup_api_explain_%'");
  //while ($row = db_fetch_object($result)) {
    //variable_del($row->name);
  //}*/
}

/**
 * Implementation of hook_update_N().
 */
function profile_setup_api_update_6000() {
  $ret = array();
  //make module be called last so the registration redirection doesn't mess with other modules
  db_query("UPDATE {system} SET weight = 100 WHERE name = 'profile_setup_api'");
  return $ret;
}
 
